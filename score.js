var scoreState = {
    preload: function() {
        game.load.image('star', 'img/star.png');
    },
    create: function() {
        game.stage.backgroundColor = '#183a5a';
        this.deleteButton = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        this.enterButton = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        if(game.global.success) this.gameoverLabel = game.add.text(game.width/2, 180, 'YOU ARE THE HERO!', { font: '60px Luminari', fill: '#b45c41'});
        else    this.gameoverLabel = game.add.text(game.width/2, 180, 'GAME OVER', { font: '80px Luminari', fill: '#b45c41'});
        this.gameoverLabel.scale.setTo(0);
        this.gameoverLabel.anchor.setTo(0.5, 0.5);
        this.scoreLabel = game.add.text(game.width/2, 300, 'SCORE: ' + game.global.score, { font: '50px monospace', fill: '#ffffff'})
        this.enterLabel = game.add.text(game.width/2, 400, 'ENTER YOUR NAME: ', { font: '30px monospace', fill: '#ffffff'});
        this.nameLabel = game.add.text(game.width/2, 450, '', { font: '30px monospace', fill: '#e2b365'});
        this.scoreLabel.anchor.setTo(0.5, 0.5);
        this.enterLabel.anchor.setTo(0.5, 0.5);
        this.nameLabel.anchor.setTo(0.5, 0.5);
        this.deleteButton.onDown.add(() => {this.deleteLast()});
        this.enterButton.onDown.add(() => {this.enterName()});

        this.emitter = game.add.emitter(0, 0, 15);
        this.emitter.setYSpeed(-400, 400);
        this.emitter.setXSpeed(-400, 400);
        this.emitter.gravity = 100;
        this.emitter.setAlpha(0.8, 1, 3000);
        this.emitter.makeParticles('star');
        this.emitter.setScale(1, 3, 1, 3, 800, Phaser.Easing.Quintic.Out);

        game.add.tween(this.gameoverLabel.scale).to({x: 1, y: 1}, 700).easing(Phaser.Easing.Bounce.Out).start();
        game.add.tween(this.gameoverLabel).to({angle: -2.5}, 800).to({angle: 2.5}, 1600).to({angle: 0}, 800).loop().start();
        game.input.keyboard.addCallbacks(this, null, null, this.keyPress);
        if(game.global.success) game.time.events.loop(800, this.emmit, this);
    },
    update: function() {

    },
    keyPress: function(char) {
        this.nameLabel.text += char;
    },
    deleteLast: function() {
        let str = "";
        for(let i=0; i<this.nameLabel.text.length - 1; i++){
            str += this.nameLabel.text[i];
        }
        this.nameLabel.text = str;
    },
    enterName: function() {
        let key = firebase.database().ref('score').push().key;
        firebase.database().ref('score/' + key).set(game.global.score);
        if(this.nameLabel.text == "")    firebase.database().ref('name/' + key).set("(No Name)");
        else    firebase.database().ref('name/' + key).set(this.nameLabel.text);
        game.state.start('over');
    },
    emmit: function() {
        let rdnX = game.rnd.integerInRange(100, 500);
        let rdnY = game.rnd.integerInRange(100, 500);
        this.emitter.x = rdnX;
        this.emitter.y = rdnY;
        this.emitter.start(true, 1000, null, 30);
    }
};

