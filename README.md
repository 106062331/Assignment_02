# Software Studio 2019 Spring Assignment 02

## Topic

- Project Name : [Assignment 02]

- Basic functions

  1. Complete game process
  2. Basic rules
  3. Jucify mechanisms
  4. Animations
  5. Particle Systems
  6. Sound effects
  7. UI
  8. Leaderboard
  9. Appearance

  Other functions (add/delete)

  1. Enhanced function
  2. Boss


## Basic Components

|       Component       | Score | Y/N  |
| :-------------------: | :---: | :--: |
| Complete game process |  15%  |  Y   |
|      Basic rules      |  20%  |  Y   |
|   Jucify mechanisms   |  15%  |  Y   |
|      Animations       |  10%  |  Y   |
|   Particle Systems    |  10%  |  Y   |
|  	 Sound Effects	    |  5%  |  Y   |
|   UI    |  5%  |  Y   |
|   Leaderboard    |  5%  |  Y   |
|   Appearance    |  5%  |  Y   |

## Advanced Components

|     Component     | Score | Y/N  |
| :---------------: | :---: | :--: |
| Multi-player game |   -   |  N   |
|  Enhanced items   |  5%   |  Y   |
|       Boss        |  5%   |  Y   |

## Website Detail Description

# 作品網址：[https://106062331.gitlab.io/Assignment_02]

# Components Description : 

## 1.	Complete game process



```mermaid
graph LR
    MENU --> GAME
    MENU --> RANKING
    GAME --> SCORE
    SCORE -->  SELECT
    SELECT --> MENU
    SELECT --> GAME
    
```

- **MENU:**  首頁。按下enter或play標誌開始遊戲，按下ranking標誌顯示遊戲得分。

- **RANKING:** Leaderboard，顯示遊戲排名（1～5名）。

- **GAME:** 主要遊戲畫面。

- **SCORE:**  顯示遊戲得分，輸入名字後按下enter，會將名字及得分上傳至firebase。

- **SELECT:** 選擇要重新遊戲或是進入MENU。



## 2.	Basic Rules

- **PLAYER:**  左右鍵移動。空白鍵攻擊。觸碰到敵人或是敵人的子彈時會受傷，受傷後會閃爍，而閃爍期間不會再遭受攻擊。
- **ENEMY:** 會隨機的左右移動，且隨機發射子彈。
- **MAP:** 地圖會捲動。

## 3.	Jucify Mechanisms

- **LEVEL:** 有三個LEVEL，LEVEL 1 敵人較少，且攻擊次數較少。LEVEL 2 敵人增加，攻擊次數增加，且子彈會朝玩家地方向發射，敵人也會偏向玩家位置前進。LEVEL 3 是 BOSS。打倒BOSS之後遊戲結束。
- **ULTIMATE SKILLS:** 遊戲進行途中會隨機掉下火的icon，接住三個icon之後就可以使用特殊攻擊（按下E，敵人會全部爆炸）。而遊戲右上角會顯示現在收集到幾個火苗。

## 4.	Animations

- **PLAYER:** 左右移動時player有動畫。
- **ENEMY:** 敵人移動時有動畫。
- **EXPLOSION:** 爆炸時有動畫（玩家受傷，敵人受傷或BOSS被打倒）。

## 5.	Particle Systems

**EMITTER** 

- BOSS被攻擊時
- 接到道具時
- 打倒BOSS後的SCORE畫面有小煙火

## 6.	Sound Effects

- 遊戲開始後有背景音樂
- 爆炸音效

## 7.	UI

- 玩家生命值
- 分數
- 收集到的火苗數量（用來發動特殊攻擊）
- 等級

## 8.	Leaderboard & Score

- **Leaderboard:** 在首頁按下RANKING圖示就可以進入Leaderboard。會顯示前五名的排名。
- **Score:** 遊戲結束後會進入Score page，會顯示分數，且可以輸入名字。因為delete鍵回上頁，因次用左鍵代替delete。按下enter後進入Select Page，資料會上傳至Firebase。如果為輸入就按下enter，名字會存為(No Name)。
- Database的存法：將score和player name分開存，但同一筆資料key會相同。這樣是為了能夠使用firebase的orderByValue().limitToLast(5)的語法，就不需要自己sort分數資料，且讓同一個Player Name重複遊戲時紀錄不會被蓋掉。
- 在排行出來前會顯示LOADING...

```mermaid
graph LR
score --> key:integer
name --> key:string

```



## 9.	Pause & Volume

- 按下P可以暫停。暫停之後有音量大小button可以更改音量或靜音。
- 按下M可以靜音。


# Bonus Description(1~10%) : 

## 1. Enhanced Items

-  **HEAL:** 被消滅的敵人可能會掉下藥丸道具，接住後可以增加1血量。
- **BULLETS:** 被消滅的敵人可能會掉下子彈道具，接住後會得到更厲害的子彈。（遊戲開始時只能發射一排子彈，接住一個子彈道具後可以發射兩排子彈。最多能夠發射三排。）

## 2.	Boss

- 血量較多。會同時發射大量炸彈。