var overState = {
    preload: function() {

    },
    create: function() {
        game.stage.backgroundColor = '#b45c41';
        this.againLabel = game.add.text(game.width/2, 300, 'play again', { font: '30px Luminari', fill: '#000000'});
        this.menuLabel = game.add.text(game.width/2, 350, 'menu', { font: '30px Luminari', fill: '#000000'});
        this.againLabel.anchor.setTo(0.5, 0.5);
        this.menuLabel.anchor.setTo(0.5, 0.5);

        this.upButton = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        this.downButton = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        this.enterButton = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.select = 0;
        this.downButton.onDown.add(() => {this.select = (this.select + 1) % 3});
        this.upButton.onDown.add(() => {this.select = (this.select > 0) ? this.select - 1 : 2});

        firebase.database().ref().push(game.global.score);
    },
    update: function() {
        if(this.select == 0) {
            this.againLabel.fontWeight = 'normal' ;
            this.menuLabel.fontWeight = 'normal';
        }
        else if(this.select == 1) {
            this.againLabel.fontWeight = 'bold';
            this.menuLabel.fontWeight = 'normal';
            if(this.enterButton.isDown) game.state.start('main');
        }
        else if(this.select == 2) {
            this.againLabel.fontWeight = 'normal';
            this.menuLabel.fontWeight = 'bold';
            if(this.enterButton.isDown) game.state.start('menu');
        }
    }
};