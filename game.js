var mainState = {
    preload: function() {
        game.load.image('bg', 'img/background0.jpg');
        game.load.image('player', 'img/player.png');
        game.load.image('bullet', 'img/bullet.png');
        game.load.image('bomb', 'img/bomb1.png');
        game.load.image('star', 'img/star.png');
        game.load.image('heart', 'img/heart.png');
        game.load.image('pixel', 'img/pixel.png')
        game.load.image('boss', 'img/boss.png');
        game.load.image('heal', 'img/heal.png');
        game.load.image('alpha', 'img/alpha.png');
        game.load.image('mute', 'img/mute.png');
        game.load.image('volUp', 'img/volumeup.png');
        game.load.image('volDown', 'img/volumedown.png');
        game.load.image('bullets', 'img/bullets.png');
        game.load.image('fire', 'img/fire.png');
        game.load.image('firegrey', 'img/firegrey.png');
        game.load.spritesheet('explosion', 'img/explosion.png', 64, 64);
        game.load.spritesheet('playersheet', 'img/playerSheet.png', 70, 70);
        game.load.spritesheet('enemysheet', 'img/enemySheet.png', 40, 40);
        game.load.audio('bombsound', 'bombsound.ogg');
        game.load.audio('bgmusic', 'backgroundmusic.ogg');
    },
    create: function() {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        this.cursor = game.input.keyboard.createCursorKeys();
        
        // this.bg1 = game.add.image(0, 0, 'bg');
        // this.bg2 = game.add.image(0, -991, 'bg');
        this.bg = game.add.tileSprite(0, 0, 660, 600, 'bg');
        this.player = game.add.sprite(game.width/2, game.height - 100, 'playersheet');
        this.player.anchor.setTo(0.5, 0);
        this.player.maxHealth = 5;
        this.player.setHealth(5);
        game.physics.arcade.enable(this.player);

        this.player.animations.add('toLeft', [1, 2], 8, false);
        this.player.animations.add('toRight', [3, 4], 8, false);
        
        game.global.success = false;
        this.levelCnt = 0;
        this.level = 1;
        this.score = 0;
        this.hurting = false;
        this.weaponLevel = 0;
        this.livingEnemies = [];
        this.firingTimer = 0;
        this.levelTimer = 0;
        this.newEnemyFlg = true;
        this.winTimer = 0;
        this.isPaused = false;
        this.volume = 0;
        this.fire = 0;

        this.fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.weapon = this.add.weapon(100, 'bullet');
        this.weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.weapon.bulletSpeed = 600;
        this.weapon.fireRate = 100;
        this.weapon.trackSprite(this.player, 0, 0, false);
        this.weaponleft = this.add.weapon(100, 'bullet');
        this.weaponleft.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.weaponleft.bulletSpeed = 600;
        this.weaponleft.fireRate = 100;
        this.weaponleft.trackSprite(this.player, -15, 0, false);
        this.weaponright = this.add.weapon(100, 'bullet');
        this.weaponright.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.weaponright.bulletSpeed = 600;
        this.weaponright.fireRate = 100;
        this.weaponright.trackSprite(this.player, 15, 0, false);
        this.weaponEnemy = this.add.weapon(100, 'bomb');
        this.weaponEnemy.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        this.weaponEnemy.bulletSpeed = -300;
        this.weaponEnemy.fireRate = 100;
        this.weaponEnemy.bulletAngleVariance = 30;
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyBullets.createMultiple(30, 'bomb');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 1);
        this.enemyBullets.setAll('outOfBoundsKill', true);
        this.enemyBullets.setAll('checkWorldBounds', true);

        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(100, 'enemysheet');
        this.enemies.forEach(function(enemy){
            enemy.animations.add('fly', [ 0, 1, 2, 3 ], 10, true);
            enemy.play('fly');
        });
        this.boss = game.add.sprite(game.width/2, -100, 'boss');
        this.boss.anchor.setTo(0.5, 0.5);
        this.boss.maxHealth = 200;
        this.boss.setHealth(200);
        game.physics.arcade.enable(this.boss);

        this.explosion = game.add.group();
        this.explosion.createMultiple(100, 'explosion');
        this.explosion.forEach(this.explosionSet, this);

        this.staremitter = game.add.emitter(0, 0, 10);
        this.staremitter.setYSpeed(-200, 200);
        this.staremitter.setXSpeed(-200, 200);
        this.staremitter.gravity = -200;
        this.staremitter.setAlpha(0.8, 1, 3000);
        this.staremitter.makeParticles('star');
        this.staremitter.setScale(1, 3, 1, 3, 800, Phaser.Easing.Quintic.Out);
        this.heartemitter = game.add.emitter(0, 0, 10);
        this.heartemitter.setYSpeed(-200, 200);
        this.heartemitter.setXSpeed(-200, 200);
        this.heartemitter.gravity = -200;
        this.heartemitter.setAlpha(0.8, 1, 3000);
        this.heartemitter.makeParticles('heart');
        this.heartemitter.setScale(0.2, 1, 0.2, 1, 800, Phaser.Easing.Quintic.Out);
        this.fireemitter = game.add.emitter(0, 0, 10);
        this.fireemitter.setYSpeed(-200, 200);
        this.fireemitter.setXSpeed(-200, 200);
        this.fireemitter.gravity = -200;
        this.fireemitter.setRotation(0, 0);
        this.fireemitter.setAlpha(0.5, 1, 3000);
        this.fireemitter.makeParticles('fire');
        this.fireemitter.setScale(0.3, 1, 0.3, 1, 800, Phaser.Easing.Quintic.Out);
        this.enemyemitter = game.add.emitter(0, 0, 10);
        this.enemyemitter.setYSpeed(-400, 400);
        this.enemyemitter.setXSpeed(-400, 400);
        this.enemyemitter.gravity = -200;
        this.enemyemitter.setRotation(0, 0);
        this.enemyemitter.setAlpha(0.5, 1, 3000);
        this.enemyemitter.makeParticles('pixel');
        this.enemyemitter.setScale(0.3, 1, 0.3, 1, 800, Phaser.Easing.Quintic.Out);

        this.healthBar = game.add.group();
        this.healthBar.createMultiple(5, 'heart');
        this.showHealthBar(this.player.health);
        this.fireBar = game.add.group();
        this.fireBar.enableBody = true;
        this.fireBar.createMultiple(5, 'fire');
        this.firegreyBar = game.add.group();
        this.firegreyBar.enableBody = true;
        this.firegreyBar.createMultiple(3, 'firegrey');
        this.showFireBar(this.fire);

        this.heal = game.add.group();
        this.heal.enableBody = true;
        this.heal.createMultiple(2, 'heal');
        this.bullets = game.add.group();
        this.bullets.enableBody = true;
        this.bullets.createMultiple(1, 'bullets');

        this.scoreLabel = game.add.text(30, 80, 'SCORE: 0', { font: '20px monospace', fill: '#ffffff'});
        this.levelLabel = game.add.text(540, 90, 'LEVEL1', {font: '80px monospace', fill: '#ffffff'});
        this.levelLabel.scale.setTo(0.3);
        this.levelLabel.anchor.setTo(0.5, 0.5);
        
        this.bgmusic = game.add.audio('bgmusic');
        this.bombsound = game.add.audio('bombsound');
        this.bgmusic.play();
        this.bgmusic.loopFull();
        
        game.input.keyboard.addCallbacks(this, null, null, this.keyPress);
        game.time.events.loop(1000, this.Level, this);
        game.time.events.loop(3000, this.dropFire, this);
    },
    keyPress: function(char) {
        if(char == 'P' || char == 'p')  this.pauseGame();
        else if(char == 'M' || char == 'm') this.muteSound();
        else if(char == 'E' || char == 'e') this.ultimateSkill();
    },
    update: function() {
        this.movePlayer();
        this.bgScroll();
        // this.bgScroll(this.bg2);
        if (game.time.now > this.firingTimer)    this.enemyFires();
        if (game.time.now > this.winTimer && this.winTimer > 0){
            this.muteSound();
            game.global.success = true;
            game.global.score = this.score;
            game.state.start('score');
        }
        if(!this.hurting){
            game.physics.arcade.overlap(this.player, this.enemies, this.hurtPlayer, null, this);
            game.physics.arcade.overlap(this.player, this.weaponEnemy.bullets, this.hurtPlayer, null, this);
            game.physics.arcade.overlap(this.enemyBullets, this.player, this.hurtPlayer, null, this);
        }
        game.physics.arcade.overlap(this.weapon.bullets, this.enemies, this.hurtEnemy, null, this);
        game.physics.arcade.overlap(this.weaponleft.bullets, this.enemies, this.hurtEnemy, null, this);
        game.physics.arcade.overlap(this.weaponright .bullets, this.enemies, this.hurtEnemy, null, this);
        game.physics.arcade.overlap(this.weapon.bullets, this.boss, this.hurtBoss, null, this);
        game.physics.arcade.overlap(this.weaponleft.bullets, this.boss, this.hurtBoss, null, this);
        game.physics.arcade.overlap(this.weaponright.bullets, this.boss, this.hurtBoss, null, this);
        game.physics.arcade.overlap(this.player, this.heal, this.healPlayer, null, this);
        game.physics.arcade.overlap(this.player, this.bullets, this.weaponLevelUp, null, this);
        game.physics.arcade.overlap(this.player, this.fireBar, this.fireUp, null, this);
    },
    movePlayer: function() {
        if(this.fireButton.isDown){
            if(this.weaponLevel == 0)   this.weapon.fire();
            else if(this.weaponLevel == 1){
                this.weaponleft.fire();
                this.weaponright.fire();
            }
            else{
                this.weapon.fire();
                this.weaponleft.fire();
                this.weaponright.fire();
            }
        }
        if(this.cursor.right.isDown){
            if(this.player.x < 620) this.player.x += 7;
            else  this.player.x = 620;
            this.player.animations.play('toRight');
        }
        else if(this.cursor.left.isDown){
            if(this.player.x > 40) this.player.x -= 7;
            else  this.player.x = 40;
            this.player.animations.play('toLeft');
        }
        else{
            this.player.animations.stop(null, true);
            // this.player.animations.stop('toRight');
        }
        if(this.cursor.down.isDown){
            if(this.player.y < 520) this.player.y += 7;
            else  this.player.y = 520;
        }
        else if(this.cursor.up.isDown){
            if(this.player.y > 20) this.player.y -= 7;
            else  this.player.y = 20;
        }
    },
    bgScroll: function() {
        this.bg.tilePosition.y += 1;
    },
    Level: function() {
        switch(this.level) {
            case 1:
                if(this.score < 50)    this.enemy1();
                this.enemyVelocityX();
                let enemyAlive1 = this.enemies.getFirstAlive();
                if(!enemyAlive1){
                    this.level += 1;
                    this.nextLevel();
                    this.newEnemyFlg = true;
                }
                break;
            case 2:
                if(this.score < 150)    this.enemy2();
                this.enemyVelocityX();
                let enemyAlive2 = this.enemies.getFirstAlive();
                if(!enemyAlive2){
                    this.level += 1;
                    this.nextLevel();
                    this.newEnemyFlg = true;
                }
                break;
            case 3:
                if(this.newEnemyFlg)    this.enemyBoss();
                break;
        }
    },
    nextLevel: function() {
        if(this.level == 3) this.levelLabel.text = "BOSS !!!";
        else  this.levelLabel.text = "LEVEL" + this.level;
        this.levelLabel.reset(game.width/2, -50);
        this.levelLabel.scale.setTo(1);
        let levelTween = game.add.tween(this.levelLabel);
        levelTween.onComplete.add(function(){this.levelLabel.reset(540, 90); this.levelLabel.scale.setTo(0.3)}, this);
        levelTween.to({y: game.height/2}, 1000).easing(Phaser.Easing.Bounce.Out).start();
    },
    enemy1: function() {
        this.newEnemyFlg = false;
        for(let i=1; i<=3; i++) {
            var enemy = this.enemies.getFirstDead();
            if(!enemy)  return;
            let random = game.rnd.integerInRange(-80, 80);
            enemy.anchor.setTo(0.5, 1);
            enemy.reset(i*200, 0);
            enemy.body.velocity.x = random;
            enemy.body.velocity.y = 80;
            enemy.checkWorldBounds = true;
            enemy.outOfBoundsKill = true;
            enemy.maxHealth = 1;
            enemy.setHealth(1);
        }
    },
    enemy2: function() {
        this.newEnemyFlg = false;
        for(let i=1; i<=7; i++) {
            var enemy = this.enemies.getFirstDead();
            if(!enemy)  return;
            enemy.anchor.setTo(0.5, 1);
            enemy.reset(i*85, 0);
            enemy.body.velocity.y = 100;
            enemy.checkWorldBounds = true;
            enemy.outOfBoundsKill = true;
            enemy.maxHealth = 1;
            enemy.setHealth(1);
        }
    },
    enemyBoss: function() {
        this.newEnemyFlg = false;
        this.boss.y = 100;
        this.weaponEnemy.trackSprite(this.boss, 0, 0, false);
        this.weaponEnemy.bulletAngleVariance = 90;
        game.time.events.loop(50, this.bossFires, this);
        let bossTween = game.add.tween(this.boss);
        // bossTween.onComplete.add(function(){this.levelLabel.reset(540, 30); this.levelLabel.scale.setTo(0.3)}, this);
        bossTween.to({x: 10}, 2000).to({x: 580}, 4000).to({x: game.width/2}, 2000).loop().start();
    },
    enemyVelocityX: function() {
        if(this.level == 1){
            this.enemies.forEachAlive((enemy) => {
                let random = game.rnd.integerInRange(-80, 80);
                enemy.body.velocity.x = random;
            });
        }
        else if(this.level == 2){
            this.enemies.forEachAlive((enemy) => {
                let random = game.rnd.integerInRange(1, 3);
                // let vx = (this.player.body.x - enemy.body.x) / random;
                if(this.player.body.x > enemy.body.y)   vx = 10*random;
                else    vx = -10*random;
                enemy.body.velocity.x = vx;
            })
        }
    },
    bossFires: function() {
        this.weaponEnemy.fire();
    },
    explosionSet: function(obj) {
        obj.anchor.x = 0;
        obj.anchor.y = 0;
        obj.animations.add('explosion');
    },
    hurtPlayer: function(player, enemy) {
        if(player.health == 1){
            this.muteSound();
            game.global.score = this.score;
            game.state.start('score');
        }
        enemy.kill();
        this.hurting = true;
        let tweenHurt = game.add.tween(player);
        tweenHurt.onComplete.add(function(){player.alpha = 1; this.hurting = false;}, this);
        tweenHurt.to({alpha: 0}, 150).yoyo(true).repeat(3).start();
        player.damage(1);
        let explosion = this.explosion.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('explosion', 15, false, true);
        this.bombsound.play();
        this.showHealthBar(player.health);
    },
    hurtEnemy: function(bullet, enemy) {
        this.score += 1;
        this.scoreLabel.text = "SCORE: " + this.score;
        if(this.score%50 < 3)  this.dropHeal(enemy);
        if(this.score%60 < 3)  this.dropBullets(enemy);
        if(enemy.health == 1){
            enemy.kill();
            let explosion = this.explosion.getFirstExists(false);
            explosion.reset(enemy.body.x, enemy.body.y);
            explosion.play('explosion', 15, false, true);
            this.bombsound.play();
            if(bullet)  bullet.kill();
        }
        else{
            enemy.damage(1);
            if(bullet)  bullet.kill();
        }
    },
    hurtBoss: function(enemy, bullet) {
        this.score += 1;
        this.scoreLabel.text = "SCORE: " + this.score;
        if(this.score%50 == 0) this.dropHeal(enemy);
        if(enemy.health == 1){
            enemy.kill();
            let explosion = this.explosion.getFirstExists(false);
            explosion.reset(enemy.body.x + 30, enemy.body.y + 30);
            explosion.scale.setTo(3);
            explosion.play('explosion', 15, false, true);
            bullet.kill();
            this.winTimer = game.time.now + 1000;
            this.hurting = true;
        }
        else{
            this.enemyemitter.x = bullet.x;
            this.enemyemitter.y = enemy.body.y + 35;
            this.enemyemitter.start(true, 50, null, 20);    
            enemy.damage(1);
            bullet.kill();
        }
    },
    enemyFires: function() {
        let enemyBullet = this.enemyBullets.getFirstExists(false);
        this.livingEnemies.length=0;
        this.enemies.forEachAlive((enemy) => {
            this.livingEnemies.push(enemy);
        });
        if (this.livingEnemies.length > 0)
        {
            let random = game.rnd.integerInRange(0, this.livingEnemies.length-1);
            let shooter = this.livingEnemies[random];
            // this.weaponEnemy.trackSprite(shooter, 0, 0, false);
            // this.weaponEnemy.fire();
            enemyBullet.reset(shooter.body.x, shooter.body.y);
            game.physics.arcade.moveToObject(enemyBullet, this.player, 300);
            this.firingTimer = game.time.now + 1500 / this.level;
        }
    },    
    showHealthBar: function(health) {
        this.healthBar.forEach((child) => {child.kill()})
        for(let i=0; i<health; i++){
            let heart = this.healthBar.getFirstDead();
            if(heart) heart.reset(30+i*25, 30);
        }
    },
    showFireBar: function(fire) {
        this.fireBar.forEach((child) => {child.kill()});
        this.firegreyBar.forEach((child) => {child.kill()})
        for(let i=0; i<fire; i++){
            let fireicon = this.fireBar.getFirstDead();
            if(fireicon) fireicon.reset(480 + i*40, 20);
        }
        for(let i=fire; i<3; i++){
            let firegreyicon = this.firegreyBar.getFirstDead();
            if(firegreyicon) firegreyicon.reset(480 + i*40, 20);
        }
    },
    dropHeal: function(enemy) {
        var heal = this.heal.getFirstDead();
        if(!heal)  return;
        heal.reset(enemy.body.x, enemy.body.y);
        heal.body.velocity.y = 300;
        heal.checkWorldBounds = true;
        heal.outOfBoundsKill = true;
    },
    dropBullets: function(enemy) {
        let bullets = this.bullets.getFirstDead();
        if(!bullets)  return;
        bullets.reset(enemy.body.x, enemy.body.y);
        bullets.body.velocity.y = 250;
        bullets.checkWorldBounds = true;
        bullets.outOfBoundsKill = true;
    },
    dropFire: function() {
        let fire = this.fireBar.getFirstDead();
        let random = game.rnd.integerInRange(100, 500);
        if(!fire) return;
        fire.reset(random, 0);
        fire.body.velocity.y = 350;
        fire.checkWorldBounds = true;
        fire.outOfBoundsKill = true;
    },
    healPlayer: function(player, heal) {
        this.heartemitter.x = player.body.x;
        this.heartemitter.y = player.body.y + 35;
        this.heartemitter.start(true, 500, null, 20);

        player.heal(1);
        heal.kill();
        this.showHealthBar(player.health);
    },
    weaponLevelUp: function(player, bullets) {
        this.staremitter.x = player.body.x;
        this.staremitter.y = player.body.y + 35;
        this.staremitter.start(true, 500, null, 20);
        this.weaponLevel += 1;
        bullets.kill();
    },
    fireUp: function(player, fire) {
        fire.kill();
        this.fireemitter.x = player.body.x + 35;
        this.fireemitter.y = player.body.y + 35;
        this.fireemitter.start(true, 500, null, 20);
        this.fire = (this.fire > 2) ? 3 : this.fire + 1;
        this.showFireBar(this.fire);
    },
    pauseGame: function() {
        this.isPaused = !this.isPaused;
        if(this.isPaused) {
            game.paused = true;
            this.alpha = game.add.image(0, 0, 'alpha');
            this.alpha.alpha = 0.3;
            this.mute = game.add.sprite(game.width/2 - 110, game.height/2, 'mute');
            this.volup = game.add.sprite(game.width/2 - 25, game.height/2, 'volUp');
            this.voldown = game.add.sprite(game.width/2 + 60, game.height/2, 'volDown');
            this.pauseLabel = game.add.text(game.width/2, game.height/2 - 100, 'PAUSE', { font: '50px monospace', fill: '#ffffff'});
            this.volLabel = game.add.text(game.width/2, game.height/2 + 80, 'VOLUME: ' + Math.round(mainState.bgmusic.volume * 10)/10, { font: '25px monospace', fill: '#ffffff'});
            this.volLabel.anchor.setTo(0.5, 0.5);
            this.pauseLabel.anchor.setTo(0.5, 0.5);
            this.mute.inputEnabled = true;
            this.volup.inputEnabled = true;
            this.voldown.inputEnabled = true;
            this.mute.events.onInputDown.add(function() {
                mainState.bgmusic.volume = 0;
                mainState.bombsound.volume = 0;    
                this.volLabel.text = 'VOLUME: 0';
            });
            this.volup.events.onInputDown.add(function() {
                mainState.bgmusic.volume += 0.2;
                mainState.bombsound.volume += 0.2;    
                this.volLabel.text = 'VOLUME: ' + Math.round(mainState.bgmusic.volume * 10)/10;
            });
            this.voldown.events.onInputDown.add(function() {
                mainState.bgmusic.volume -= 0.2;
                mainState.bombsound.volume -= 0.2;    
                this.volLabel.text = 'VOLUME: ' + Math.round(mainState.bgmusic.volume * 10)/10;
            });
        }
        else {
            game.paused = false;
            this.alpha.destroy();
            this.pauseLabel.destroy();
            this.mute.destroy();
            this.volup.destroy();
            this.voldown.destroy();
            this.volLabel.destroy();
        }
    },
    muteSound: function() {
        this.bgmusic.volume = 0;
        this.bombsound.volume = 0;    
    },
    ultimateSkill: function() {
        if(this.fire == 3) {
            this.fire = 0;
            this.showFireBar(this.fire);
            this.enemies.forEachAlive((enemy) => {this.hurtEnemy(null, enemy);});
        }
    }
 };