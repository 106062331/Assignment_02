var menuState = {
    preload: function() {
        game.load.image('menubg', 'img/menubackground.png');
        game.load.image('start', 'img/play-button.png');
        game.load.image('rank', 'img/ranking.png');
    },
    create: function() {
        game.add.image(0, 0, 'menubg');
        this.menuLabel = game.add.text(game.width/2, 180, 'MENU', { font: '80px Gill Sans', fill: '#183a5a'});
        this.menuLabel.anchor.setTo(0.5, 0.5);
        this.menuLabel.fontWeight = 'bold';
        this.menuLabel.stroke = '#e2b365';
        this.menuLabel.strokeThickness = 30;
        game.add.tween(this.menuLabel).to({angle: -2.5}, 800).to({angle: 2.5}, 1600).to({angle: 0}, 800).loop().start();
        
        this.enterButton = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        this.startSign = game.add.button(game.width/2 - 90, 350, 'start', this.clickStart, this, 2, 1, 0);
        this.startSign.scale.setTo(0.8);
        this.startSign.anchor.setTo(0.5, 0.5);
        this.startSign.onInputOver.add(this.overstart, this);
        this.startSign.onInputOut.add(this.outstart, this);
        this.startSign.onInputUp.add(this.upstart, this);

        this.rankSign = game.add.button(game.width/2 + 70, 350, 'rank', this.clickStart, this, 2, 1, 0);
        this.rankSign.scale.setTo(0.85);
        this.rankSign.anchor.setTo(0.5, 0.5);
        this.rankSign.onInputOver.add(this.overrank, this);
        this.rankSign.onInputOut.add(this.outrank, this);
        this.rankSign.onInputUp.add(this.uprank, this);

    },
    update: function() {
        if(this.enterButton.isDown)   game.state.start('main');
    },
    overstart: function() {
        game.add.tween(this.startSign).to({angle: -20}, 100).start();
        game.add.tween(this.startSign.scale).to({x: 1, y: 1}, 100).start();
    },
    outstart: function() {
        game.add.tween(this.startSign).to({angle: 0}, 100).start();
        game.add.tween(this.startSign.scale).to({x: 0.8, y: 0.8}, 100).start();
    },
    upstart: function() {
        game.state.start('main');
    },
    overrank: function() {
        game.add.tween(this.rankSign).to({angle: -20}, 100).start();
        game.add.tween(this.rankSign.scale).to({x: 1, y: 1}, 100).start();
    },
    outrank: function() {
        game.add.tween(this.rankSign).to({angle: 0}, 100).start();
        game.add.tween(this.rankSign.scale).to({x: 0.8, y: 0.8}, 100).start();
    },
    uprank: function() {
        game.state.start('rank');
    }
};


var game = new Phaser.Game(660, 600, Phaser.AUTO, "canvas")
game.global = {
    score: 0,
    success: false
};
game.state.add('score', scoreState);
game.state.add('menu', menuState);
game.state.add('rank', rankState);
game.state.add('main', mainState);
game.state.add('over', overState);
game.state.start('menu');


