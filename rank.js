var rankState = {
    preload: function() {
        game.load.image('ranking', 'img/ranking.png');
        game.load.image('return', 'img/return.png');
    },
    create: function() {
        game.stage.backgroundColor = '#183a5a';
        game.add.image(80, 80, 'ranking');
        // game.add.image(50, game.height-80, 'return');
        this.loadingLabel = game.add.text(game.width/2, 300, "LOADING...", { font: '30px monospace', fill: '#ffffff'});
        this.rankingLabel = game.add.text(game.width/2 + 20, 130, "RANKING", { font: '80px monospace', fill: '#e2b365'});
        this.rankingLabel.anchor.setTo(0.5, 0.5);
        this.loadingLabel.anchor.setTo(0.5, 0.5);
        this.rankingLabel.fontWeight = 'bold';
        game.add.tween(this.rankingLabel).to({angle: -2.5}, 800).to({angle: 2.5}, 1600).to({angle: 0}, 800).loop().start();
        firebase.database().ref('score').orderByValue().limitToLast(5).once('value').then((snapshot) => {
            // console.log(snapshot.numChildren());
            this.loadingLabel.destroy();
            let i = 5;
            snapshot.forEach((scoredata) => {
                firebase.database().ref('name/' + scoredata.key).once('value').then((namedata) => {
                    let topLabel = game.add.text(game.width/2 - 200, 200 + i*60, "TOP " + i, { font: '30px monospace', fill: '#e2b365'});
                    let nameLabel = game.add.text(game.width/2, 200 + i*60, namedata.val(), { font: '30px monospace', fill: '#ffffff'});
                    let scoreLabel = game.add.text(game.width/2 + 200, 200 + i*60, scoredata.val(), { font: '30px monospace', fill: '#ffffff'});
                    topLabel.fontWeight = 'bold';
                    topLabel.anchor.setTo(0.5, 0.5);
                    nameLabel.anchor.setTo(0.5, 0.5);
                    scoreLabel.anchor.setTo(0.5, 0.5);
                    i -= 1;
                });
            });
        });
        this.returnSign = game.add.button(57, game.height - 50, 'return', this.clickreturn, this, 2, 1, 0);
        this.returnSign.scale.setTo(0.8);
        this.returnSign.anchor.setTo(0.5, 0.5);
        this.returnSign.onInputOver.add(this.overreturn, this);
        this.returnSign.onInputOut.add(this.outreturn, this);
        this.returnSign.onInputUp.add(this.upreturn, this);
    },
    overreturn: function() {
        game.add.tween(this.returnSign).to({angle: -20}, 100).start();
        game.add.tween(this.returnSign.scale).to({x: 1, y: 1}, 100).start();
    },
    outreturn: function() {
        game.add.tween(this.returnSign).to({angle: 0}, 100).start();
        game.add.tween(this.returnSign.scale).to({x: 0.8, y: 0.8}, 100).start();
    },
    upreturn: function() {
        game.state.start('menu');
    }
};